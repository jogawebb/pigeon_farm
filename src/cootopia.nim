###############################################################################
########## PIGEON FARM v 0.1.0                                       ########## 
########## Developed by Gabe Webb                                    ##########
###############################################################################

# import server
# import mustache
# import redisConnection
# import asyncdispatch
import 
  types/player,
  types/items/item,
  types/items/diary,
  types/map/map

const
  ROWS = 256
  COLS = 256

# proc setup(): Player =
  # server.launchServer()
  # waitFor connectToRedis()

proc mainLoop(playerName: string): Player =
  var sergio = initPlayer playerName
  return sergio

when isMainModule:
  let newMap = generateMapValues(ROWS, COLS)
  let smoothedMap = smoothMapValues(newMap)
  echo newMap