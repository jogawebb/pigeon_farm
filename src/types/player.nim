import 
    items/item

type
    Verb* = enum
        Add = "add"
        Remove = "remove"

    State* = enum
        Waking = "awake"
        Sleeping = "asleep"
    
    Player* = ref object
        state*: State
        coordinates*: (int, int)
        money*: int64
        age*: uint8
        inventory*: seq[Item]
        name*: string
        speed*: uint8
        clothes*: seq[Item]
        address*: string
        currentHealth*: uint8
        maxHealth*: uint8
        friends*,
            enemies*: seq[Player]
        skills*: seq[string]


func initPlayer*(name: string): Player =
    let newPlayer = Player(
        state: Waking,
        coordinates: (100, 100),
        money: 100,
        age: 16,
        inventory: @[],
        name: name,
        speed: 2,
        clothes: @[],
        address: "123 Elm Street",
        currentHealth: 100,
        maxHealth: 100
    )
    return newPlayer

func updateInventory*(player: Player, verb: Verb, item: Item): Player = 
    case verb:
        of Add:
            insert(player.inventory, item)
        of Remove:
            let index = find(player.inventory, item)
            if index == -1:
                debugEcho "Item not in inventory!"
            else:
                delete(player.inventory, index)
    return player
