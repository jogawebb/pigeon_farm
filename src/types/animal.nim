import 
    pigeon,
    fox

type
    Animal* = object
        # species*: Pigeon | Fox
        coordinates*: (int, int)
        sex*: string
        age*,
            speed*,
            endurance*,
            loyalty*,
            bravery*,
            fertility*,
            intelligence*,
            currentSpeed*,
            maxSpeed*,
            vision*,
            hunger*,
            hearing*: uint8
        currentHealth*,
            maxHealth*: int16

