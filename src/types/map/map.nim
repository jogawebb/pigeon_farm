## The Map is constructed of tiles that have a position, ground
## cover and elevation. Ground cover and elevation effect how well
## ground-moving creatures can maneuver over the tile. 

import std/random

type
    ## A map is two-dimensional colleciton of Tiles. 
    Map* = object
        width*: int
        height*: int
        day*: int
        month*: int
        year*: int
        temperature*: int
        land*: seq[seq[Tile]]
    
    Tile* = object
        coordinates*: (int, int)
        elevation*: uint8
        terrain*: TerrainType

    TerrainType* = enum
        Grass
        Dirt
        Pavement
        Rock
        Sand
        Snow
        Ice
        Water

    TerrainParameters* = tuple
        name: TerrainType
        multiplier: int
        character: string
        color: string

    ## A plot is a unit used to organize land on the map. It can be
    ## any number of contiguous tiles, greater than or equal to one.
    Plot* = object
        size*: (int, int)
        tiles*: seq[seq[Tile]]

var 
    TerrainTypes*: seq[TerrainParameters] = @[
        (Grass, 0, "╷", "ForestGreen"),
        (Dirt, 1, "▒", "SaddleBrown"),
        (Pavement, 2, "█", "DarkSlateGray"),
        (Rock, -1, "▓", "Gray"),
        (Sand, -2, "░", "Seashell"),
        (Snow, -3, "░", "LightCyan"),
        (Ice, -4, "╮", "Azure"),
        (Water, -10, "◞", "DodgerBlue")
    ]
    
## This function generates a grid of values between 0-255 which will serve
## as the basis for our map. 
func generateMapValues*(mapWidth: int, mapHeight: int): seq[seq[int]] = 
    var map = newSeq[seq[int]](mapHeight)
    # We use initRand here so that we don't have to worry about random
    # number generation creating side effects. 
    var rng = initRand(0x022378008)
    for h in 0..<mapHeight:
        var row = newSeq[int](mapWidth)    
        for w in 0..<mapWidth:
            let cellValue = rng.rand(255)
            row[w] = cellValue
        map[h] = row       
    # {.cast(noSideEffect).}:
    #     echo map
    return map

# This is a bit ugly and needs attetion, but it averages the value of a cell
# using two adjacent cells so that our terrain is kinda neatly mapped.
func smoothMapValues*(mapGrid: seq[seq[int]]): seq[seq[int]] =
    # TODO: Test that all our map's rows have the same length
    let mapWidth = mapGrid[0].len
    let mapHeight = mapGrid.len
    var smoothedMap = newSeq[seq[int]](mapHeight)

    for h in 0..<mapHeight:
        let currentRow = mapGrid[h]
        var smoothedRow = newSeq[int](mapWidth)
        case h:
            of 0:
                smoothedMap[h] = currentRow
            else:
                let previousRow = mapGrid[h - 1]
                for w in 0..<mapWidth:
                    var cellLeft, cellAbove: int
                    let unsmoothedCurrentCell = currentRow[w]
                    case w:
                        of 0:
                            cellLeft = currentRow[mapWidth - 1]
                            cellAbove = previousRow[w]
                        else:
                            cellLeft = currentRow[w - 1]
                            cellAbove = previousRow[w]
                    smoothedRow[w] = ((cellLeft + cellAbove + unsmoothedCurrentCell ) / 3).int
                smoothedMap[h] = smoothedRow
    {.cast(noSideEffect).}:
        echo smoothedMap
    return smoothedMap

# DONE: Assign a value to each cell (e.g. 1-255)
# Average it against top and left neighbors.
# To each range assign a terrain type.

    
