type
    Item* = ref object of RootObj
        name*: string
        description*: string
        weight*: float32
        volume*: float32
        currentHealth*: uint8
        totalHealth*: uint8