import item

type
    Clothes* = ref object of Item
        shirt: Shirt,

    Garment* = enum
        Shirt,
        Coat,
        Trousers,
        Underwear,
        Brazier,
        Hat,
        Watch,
        Ring,
        Necklace,
        Bracelet,
        Glasses

