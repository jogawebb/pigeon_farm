import
    item

type
    Diary* = ref object of Item
        totalPages*, 
            pagesUsed*: uint16
        content*: seq[string]

func createDiary*(diaryName: string): Diary =
    let newDiary = Diary(
        totalPages: 365,
        pagesUsed: 0,
        name: diaryName,
        description: "A diary for recording your thoughts.",
        weight: 0.5,
        volume: 0.5,
        currentHealth: 100,
        totalHealth: 100,
        content: @[]
    )
    return newDiary


