# Package

version       = "0.1.0"
author        = "jogawebb"
description   = "A new awesome nimble package"
license       = "GPL-2.0-or-later"
srcDir        = "src"
bin           = @["cootopia"]


# Dependencies

requires "nim >= 1.6.8"
